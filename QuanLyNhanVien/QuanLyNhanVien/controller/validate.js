// true  => hợp lệ

function kiemTraTrung(idNv, nvArr) {
  var index = nvArr.findIndex(function (item) {
    return idNv == item.account;
  });
  if (index == -1) {
    // show Thông báo lỗi nếu có
    document.getElementById("spanMaSV").innerText = "";
    return true;
  } else {
    // Show thông báo
    document.getElementById("spanMaSV").innerText = "Mã sinh viên đã tồn tại";
    return false;
  }
}

function kiemTraDoDai(value, idErr, min, max) {
  var length = value.length;
  if (length < min || length > max) {
    document.getElementById(
      idErr
    ).innerText = ` Độ dài phải từ ${min} đến ${max} kí tự `;
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}

function kiemTraSo(value, idErr) {
  var reg = /^\d+$/;
  document.getElementById(idErr).innerText = "";
  var isNumber = reg.test(value);
  if (isNumber) {
    document.getElementById(idErr).innerText = "";
    return true;
  } else {
    document.getElementById(idErr).innerText = "Chỉ được thêm số";
    return false;
  }
}
function kiemTraEmail(value) {
  const reg =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var email = reg.test(value);
  if (email) {
    document.getElementById("spanEmailSV").innerText = "";
    return true;
  } else {
    document.getElementById("spanEmailSV").innerText = "email không hợp lệ";
    return false;
  }
}
