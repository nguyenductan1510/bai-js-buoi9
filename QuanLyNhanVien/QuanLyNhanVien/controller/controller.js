function getInformation() {
  // getInformation()
  var _account = document.getElementById("tknv").value;
  var _name = document.getElementById("name").value;
  var _email = document.getElementById("email").value;
  var _password = document.getElementById("password").value;
  var _day = document.getElementById("datepicker").value;
  var _luongCB = document.getElementById("luongCB").value * 1;
  var _position = document.getElementById("chucvu").value * 1;
  var _time = document.getElementById("gioLam").value * 1;

  var nv = new NhanVien(
    _account,
    _name,
    _email,
    _password,
    _day,
    _luongCB,
    _position,
    _time
  );
  return nv;
}

function renderDSNV(nvArr) {
  var contentHTML = "";
  for (var index = 0; index < nvArr.length; index++) {
    var item = nvArr[index];
    var contentTr = `   <tr> 
                           <td>${item.account}</td>
                           <td>${item.name}</td>
                           <td>${item.email}</td>
                           <td>${item.day}</td>
                           <td>${"item.tongLuong()"}</td>
                           <td>${"item.xepLoai()"}</td>
                           <td> 
                           <button onclick = "xaoNhanVien('${
                             item.account
                           }')" class="btn btn-danger">Xóa</button>
                           <button onclick = "suaNhanVien('${
                             item.account
                           }')" class="btn btn-warning ">Sửa</button>
                           </td>
                        </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tbChucVu").innerHTML = contentHTML;
}

function timKiemViTri(id, arr) {
  // tìm vị trí
  var viTri = -1;

  for (var index = 0; index < arr.length; index++) {
    var nv = arr[index];
    if (nv.account == id) {
      viTri = index;
      break;
    }
  }
  return viTri;
}

function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.account;
  document.getElementById("name").value = nv.name;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.password;
  document.getElementById("datepicker").value = nv.day;
  document.getElementById("luongCB").value = nv.luongCB;
  document.getElementById("chucvu").value = nv.position;
  document.getElementById("gioLam").value = nv.time;
}
